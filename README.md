# PROCOAST2024. COURSE ON PROBABILISTIC DESIGN ON COASTAL STRUCTURES.
## Statistical modelling of environmental conditions

Paula Camus Braña (paula.camus@unican.es)\
Laura Cagigal Gil (laura.cagigal@unican.com)\
Jared Ortiz-Angulo Cantos (jared.ortizangulo@unican.es)

---
<a name="ins"></a>
## Simulation of the met-ocean conditions using jupyter notebooks in python: TESLA
This repository contains the guide and notebooks to follow the course example on the North Atlantic Ocean. The practice is divided into two modules (two cases) during which different notebooks will be used to generate and process all the data and files necessary for the successful completion of the practice.

- - -
<a name="ins"></a>
## First steps
First, we must [create](https://support.google.com/mail/answer/56256?hl=en) (or use if we already have one) a secondary gmail account separate from our personal and laboral accounts, if we are concerned about giving access to our Drive information (Google Colab and Drive will connect during the class and certain file manipulation permissions will need to be given).

Once logged in with the account, we request access to the Google Shared Drive of the course: send an email to: jared.ortizangulo@unican.es with the subject: "Request"

- - -
<a name="ins"></a>
## How to run the notebooks
Notebooks can be followed in two ways: "Locally" by installing Python and Jupyter and in The Cloud without installing anything on the computer via [Google Colab](https://colab.research.google.com/). The first way requires the prior installation of Python, as well as Jupyter and the required libraries for the execution of the notebooks (in the repository there is an environment that has the necessary libraries, however, compatibility problems may occur). On the other hand, to use google Colab you need a [Google account](https://www.google.com/intl/es/account/about/).

- - -
<a name="ins"></a>
## CASES

<a name="ins"></a>
## CASE 1: TESLA (Seasonality)

In CASE 1, we will cover a simple version of the Climate-Based Emulator TESLA. We will cover the generation of DWT (Notebook 1_01), the synthetic series of DWTs using a chronological model (Notebook 1_02) and the generation of synthetic series of predictands (Notebook 1_03).

[01_CASE_1_DWT_Colab.ipynb](https://colab.research.google.com/drive/1zCPWqsQR4Csl780MQ1b59qISYop_e--g?usp=drive_link)

[02_CASE_1_DWT_ALR_Colab.ipynb](https://colab.research.google.com/drive/1ed-myiJJ1xdxfAQV7Hrsj03f3MP7gUpO?usp=drive_link)

[03_CASE_1_CLIMATE_EMULATOR_Colab.ipynb](https://colab.research.google.com/drive/1TQ8Ysr6IQ9iSDnoNqdGlBRDh40VEefX1?usp=drive_link)


<a name="ins"></a>
## CASE 2: TESLA (AWT, Seasonality & IWT)

The second CASE is a slightly more complex version, where we will include two new notebooks, for the generation of AWT and IWT (Notebooks 2_02 and 2_03), which will be used with covariates in the ALR for the generation of synthetic DWT series.

[01_CASE_2_DWT_Colab.ipynb](https://colab.research.google.com/drive/1x1NWII5Od2AH-dMB2rsq0i3QxtG9dtA8?usp=sharing)

[02_CASE_2_AWT_Colab.ipynb](https://colab.research.google.com/drive/1tEGjGnXeNgIxDX0Zd6vF2jaOdzjS0gI3?usp=sharing)

[03_CASE_2_AWT_ALR_Colab.ipynb](https://colab.research.google.com/drive/1GcBlueCUnPLpTUzYuD8J4csHZs8G9ZHS?usp=sharing)

[04_CASE_2_MJO_IWT_ALR_Colab.ipynb](https://colab.research.google.com/drive/1wHsPj4ZiIvywdnmh7KHj9n1KfEKbrfRN?usp=sharing)

[05_CASE_2_DWT_ALR_Colab.ipynb](https://colab.research.google.com/drive/1pkFFrvkS6ZV3nHJ1YYnIRxYp2kSFvabn?usp=sharing)

[06_CASE_2_CLIMATE_EMULATOR_Colab.ipynb](https://colab.research.google.com/drive/150SpeI0OBw26QfqTrmSg37-RFc4sSsTE?usp=sharing)

- - -
<a name="ins"></a>
## Acknowledgements

The authors would like to give special thanks to the attendees of the course ProCoast 2024 hold in the Faculty of Engineering of the University of Porto (Porto, Portugal) on 18th -21th June 2024.

[GeoOcean Website](https://geoocean.unican.es/)

[GeoOcean Linkedin](https://www.linkedin.com/company/99983430/admin/feed/posts/)





